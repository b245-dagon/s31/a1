// 1. What directive is used by Node.js in loading the modules it needs?

	// http



// 2. What Node.js module contains a method for server creation?

	//require

// 3. What is the method of the http object responsible for creating a server using Node.js?

	//.createServer();

// 4. What method of the response object allows us to set status codes and content types?
	
	//.writeHead();

// 5. Where will console.log() output its contents when run in Node.js?

	// terminal


// 6. What property of the request object contains the address's endpoint?

	// the request.url 





const http = require("http");


const port = 3000;

const server = http.createServer((request , response) => {



			if(request.url == "/login"){
				response.writeHead(200,{"Content-type": "text/plain"});
				response.end("Welcome to the Login Page");

			}else{
				response.writeHead(404, {"Content-type": "text/plain"});
				response.end("Page Not Found");
			}
	});

server.listen(port);
console.log(`Server now accessible at localhost ${port}`);
